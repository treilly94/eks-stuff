resource "aws_security_group" "eks" {
  name        = "${var.name} eks cluster"
  description = "Allow traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "World"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge({
    "kubernetes.io/cluster/${var.name}" : "owned"
  }, var.tags)
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.13.1"

  cluster_name                   = var.name
  cluster_version                = "1.25"
  cluster_endpoint_public_access = true

  vpc_id                                = var.vpc_id
  subnet_ids                            = var.private_subnet_ids
  cluster_additional_security_group_ids = [aws_security_group.eks.id]

  eks_managed_node_groups = {
    green = {
      min_size     = 1
      max_size     = 2
      desired_size = 1

      instance_types         = ["t3.medium"]
      capacity_type          = "SPOT"
      vpc_security_group_ids = [aws_security_group.eks.id]
      tags                   = var.tags
    }
  }

  tags = var.tags
}
