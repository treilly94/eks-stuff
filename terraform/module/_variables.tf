variable "name" {}
variable "vpc_id" {}
variable "private_subnet_ids" {}
variable "tags" {}

variable "gitlab_group" {}
variable "gitlab_project" {}
