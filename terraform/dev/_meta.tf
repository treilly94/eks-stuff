terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.63.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = "1.0.0-rc.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=15.10.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

provider "gitlab" {
  token = var.gitlab_token
}

locals {
  name = "dev"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
