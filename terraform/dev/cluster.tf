module "cluster" {
  source = "../module"

  name               = local.name
  vpc_id             = module.vpc.vpc_id
  private_subnet_ids = module.vpc.private_subnets
  tags               = local.tags

  gitlab_group   = "treilly94"
  gitlab_project = "eks-stuff"
}
