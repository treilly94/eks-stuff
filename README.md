# eks stuff

## EKS

### Load balancer controller

[Stolen From](https://andrewtarry.com/posts/terraform-eks-alb-setup/)

## Flux

### Bootstrapping

[Stolen From](https://registry.terraform.io/providers/fluxcd/flux/latest/docs/guides/gitlab)

### Repo Layout

[Stolen From](https://github.com/fluxcd/flux2-kustomize-helm-example)
